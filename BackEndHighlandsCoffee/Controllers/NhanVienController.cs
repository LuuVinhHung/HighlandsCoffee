﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using apiModel;
using BackEndHighlandsCoffee.View_model;

namespace BackEndHighlandsCoffee.Controllers
{
    public class NhanVienController : ApiController
    {
        private ApiDbContext db;
        private ErrorModel error;

        public NhanVienController()
        {
            db = new ApiDbContext();
            error = new ErrorModel();
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var ds = db.NhanViens.Select(x => new NhanVien_model
            {
                Id = x.Id,
                HoTen = x.HoTen,
                DiaChi = x.DiaChi,
                NgaySinh = x.NgaySinh,
                GioiTinh = x.GioiTinh,
                LoaiNhanVienId = x.LoaiNhanVien.Id
            });
            return Ok(ds);
        }

        [HttpGet]
        public IHttpActionResult GetBydId(int id)
        {
            IHttpActionResult httpActionResult;

            NhanVien nhanVien = db.NhanViens.FirstOrDefault(x => x.Id == id);
            if(nhanVien == null)
            {
                error.Add("Không tìm thấy nhân viên");
                httpActionResult = Ok(error);
            } else
            {
                httpActionResult = Ok(new NhanVien_model(nhanVien));
            }
            return httpActionResult;
        }

        [HttpPost]
        public IHttpActionResult Create(TaoNhanVien nVModel)
        {
            IHttpActionResult httpActionResult;
            if(string.IsNullOrEmpty(nVModel.HoTen))
            {
                error.Add("Tên nhân viên không được phép NULL");
            }
            if(error.Errors.Count == 0)
            {
                NhanVien nhanVien = new NhanVien();
                nhanVien.HoTen = nVModel.HoTen;
                nhanVien.GioiTinh = nVModel.GioiTinh;
                nhanVien.DiaChi = nVModel.DiaChi;
                nhanVien.NgaySinh = nVModel.NgaySinh;
                //nhanVien.LoaiNhanVien = nVModel.LoaiNhanVienId;
                //nhanVien.ChiNhanh = nVModel.ChiNhanhId;

                nhanVien = db.NhanViens.Add(nhanVien);

                db.SaveChanges();
                NhanVien_model vien_Model = new NhanVien_model(nhanVien);
                httpActionResult = Ok(vien_Model);
            } else
            {
                httpActionResult = Ok(error);
            }
            return httpActionResult;
        }

        [HttpPut]
        public IHttpActionResult Update(CapNhatNhanVien nVModel)
        {
            IHttpActionResult httpActionResult;
            NhanVien nhanVien = db.NhanViens.FirstOrDefault(x => x.Id == nVModel.Id);
            if(nhanVien == null)
            {
                error.Add("Không tìm thấy nhân viên");
                httpActionResult = Ok(error);
            }
            else
            {
                nhanVien.HoTen = nVModel.HoTen ?? nVModel.HoTen;
                nhanVien.GioiTinh = nVModel.GioiTinh;
                nhanVien.DiaChi = nVModel.DiaChi ?? nVModel.DiaChi;
                nhanVien.Id = nVModel.Id;

                db.Entry(nhanVien).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                httpActionResult = Ok(new NhanVien_model(nhanVien));
            }
            return httpActionResult;
        }
    }
}
