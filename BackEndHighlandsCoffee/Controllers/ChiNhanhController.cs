﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using apiModel;
using BackEndHighlandsCoffee.View_model;



namespace BackEndHighlandsCoffee.Controllers
{
    public class ChiNhanhController : ApiController
    {

       
        private ApiDbContext db;

        private ErrorModel error;

        public ChiNhanhController()
        {
            db = new ApiDbContext();
            error = new ErrorModel();
        }


        /**
         * @api [Get] /ChiNhanh/ Get all ChiNhanh
         * @apiGroup ChiNhanh
         * @apiPermission none
         * 
         * 
         * @apiSuccess {int} Id Iid cua ChiNhanh
         * @apiSuccess {string} TenChiNhanh
         * @apiSuccess {DateTime} NgayThanhLap
         * @apiSuccess {string} DiaChi
         * 
         * @apiSuccessExample {json} Response:
         * {
         *      Id: 1,
         *      TenChiNhanh: 'HighLand 01',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         * 
         * @apiError (400) {string[]} Errors Mang cac loi
         * 
         * @apiErrorExample: {json}
         * {
         *      "Errors": [
         *      
         *      ]
         *  }
         * 
         
        */
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var ds = db.ChiNhanhs.Select(x => new ChiNhanh_model
            {
                Id = x.Id,
                TenChiNhanh = x.TenChiNhanh,
                NgayThanhLap = x.NgayThanhLap,
                DiaChi = x.DiaChi
            
            });

            return Ok(ds);
        }




        /**
         * @api [Get] /ChiNhanh/:id 
         * @apiGroup ChiNhanh
         * @apiPermission none
         *
         * @apiParam {int} Id 
         *
         * @apiParamExample {json} Request-Example:
         * {
         *      Id: 1,
         *      TenChiNhanh: 'HighLand 1',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         *
         * @apiSuccess {int} Id
         * @apiSuccess {string} TenChiNhanh  
         * @apiSuccess {DateTime} NgayThanhLap
         * @apiSuccess {string} DiaChi
         * 
         * @apiSuccessExample {json} Response:
         * {
         *      Id: 1,
         *      TenChiNhanh: 'HighLand 1',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         * 
         * 
         * @apiError (400) {string[]} Errors Mang cac loi
         * 
         * @apiErrorExample: {json}
         * {
         *      "Errors": [
         *          "Id không hợp lệ",
         *      ]
         * } 
         * 
         */
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            IHttpActionResult httpActionResult;

            ChiNhanh chiNhanh = db.ChiNhanhs.FirstOrDefault(x => x.Id == id);
            if(chiNhanh == null)
            {
                error.Add("Không tìm thấy chi nhánh");
                httpActionResult = Ok(error);
            } else
            {
                httpActionResult = Ok(new ChiNhanh_model(chiNhanh));
            }
            return httpActionResult;
        }



        /**
         * @api [Post] /ChiNhanh/ Tao mot CN moi
         * @apiGroup ChiNhanh
         * @apiPermission none
         * 
         * @apiParam {int} Id Ma cua CN moi 
         * @apiParam {string} TenChiNhanh Ten cua CN moi
         * @apiParam {string} DiaChi Dia chi cua CN moi
         * @apiParam {DateTime} NgayThanhLap NgayTL cua CN moi
         * 
         * @apiParamExample {json} Request-Example:
         * {
         *      Id: 1,
         *      TenChiNhanh: 'HighLand 1',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         * 
         * @apiSuccess {int} Id
         * @apiSuccess {string} TenChiNhanh
         * @apiSuccess {DateTime} NgayThanhLap 
         * @apiSuccess {string} DiaChi
         * 
         * @apiSuccessExample {json} Response:
         * {
         *      Id: 1,
         *      TenChiNhanh: 'HighLand 1',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         * 
         * 
         * @apiError (400) {string[]} Errors Mang cac loi
         * 
         * @apiErrorExample: {json}
         * {
         *      "Errors": [
         *          "Id la truong bat buoc",
         *          "TenChiNhanh la truong bat buoc",
         *          "DiaChi la truong bat buoc",
         *          "NgayThanhLap la truong bat buoc"
         *      ]
         * } 
         * 
         */
        [HttpPost]
        public IHttpActionResult Create(TaoChiNhanh cNModel)
        {
            IHttpActionResult httpActionResult;
            if (string.IsNullOrEmpty(cNModel.TenChiNhanh))
            {
                error.Add("Tên chi nhánh không được phép NULL");
            }

            if(error.Errors.Count == 0)
            {
                ChiNhanh chiNhanh = new ChiNhanh();
                chiNhanh.TenChiNhanh = cNModel.TenChiNhanh;
                chiNhanh.NgayThanhLap = cNModel.NgayThanhLap;
                chiNhanh.DiaChi = cNModel.DiaChi;

                chiNhanh = db.ChiNhanhs.Add(chiNhanh);

                db.SaveChanges();
                ChiNhanh_model chiNhanh_Model = new ChiNhanh_model(chiNhanh);
                httpActionResult = Ok(chiNhanh_Model);
            } else
            {
                httpActionResult = Ok(error);
            }
            return httpActionResult;
        }


        /**
         * @api [Put] /ChiNhanh/:id 
         * @apiGroup ChiNhanh
         * @apiPermission none
         *
         * @apiParam {int} Id Ma cua CN 
         *
         * @apiParamExample {json} Request-Example:
         * {
         *      Id: 1,
         *      TenChiNhanh: 'HighLand 1',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         *
         * @apiSuccess {int} Id 
         * @apiSuccess {string} TenChiNhanh 
         * @apiSuccess {DateTime} NgayThanhLap
         * @apiSuccess {string} DiaChi
         * 
         * @apiSuccessExample {json} Response:
         * {
         *       Id: 1,
         *      TenChiNhanh: 'HighLand 1',
         *      NgayThanhLap: '1/1/2018',
         *      DiaChi: '1 Nguyen Du'
         * }
         * 
         * 
         * @apiError (400) {string[]} Errors Mang cac loi
         * 
         * @apiErrorExample: {json}
         * {
         *      "Errors": [
         *          "Id không hợp lệ",
         *      ]
         * } 
         * 
         */
        [HttpPut]
        public IHttpActionResult Update(CapNhatChiNhanh cNModel)
        {
            IHttpActionResult httpActionResult;
            ChiNhanh chiNhanh = db.ChiNhanhs.FirstOrDefault(x => x.Id == cNModel.Id);
            if(chiNhanh == null)
            {
                error.Add("Không tìm thấy chi nhánh");
                httpActionResult = Ok(error);
            }else
            {

                chiNhanh.TenChiNhanh = cNModel.TenChiNhanh ?? cNModel.TenChiNhanh;
                chiNhanh.DiaChi = cNModel.DiaChi ?? cNModel.DiaChi;
                db.Entry(chiNhanh).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                httpActionResult = Ok(new ChiNhanh_model(chiNhanh));


            }
            return httpActionResult;
        }

    }
}
