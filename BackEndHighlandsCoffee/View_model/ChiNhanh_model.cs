﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using apiModel;

namespace BackEndHighlandsCoffee.View_model
{
    public class ChiNhanh_model
    {
        public int Id { get; set; }
        public string TenChiNhanh { get; set; }
        public string DiaChi { get; set; }
        public DateTime NgayThanhLap { get; set; }

        public ChiNhanh_model()
        {

        }
        public ChiNhanh_model(ChiNhanh cn)
        {
            this.Id = cn.Id;
            this.TenChiNhanh = cn.TenChiNhanh;
            this.DiaChi = cn.DiaChi;
            this.NgayThanhLap = cn.NgayThanhLap;
            
        }
        public class TaoChiNhanh
        {

            public string TenChiNhanh { get; set; }
            public string DiaChi { get; set; }
            public DateTime NgayThanhLap { get; set; }
        }
        public class CapNhatChiNhanh : TaoChiNhanh
        {
            public int Id { get; set; }
        }
        public class XoaChiNhanh : CapNhatChiNhanh
        {

        }

    }


}